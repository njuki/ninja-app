# NinjaApp

This is a test application for testing an auth api that can be found [here](https://bitbucket.org/njuki/ninja-api/src/master/)

## To test/run

Open the folder in a terminal, run `npm install` than run `ng serve`. Navigate to `http://localhost:4200/`
