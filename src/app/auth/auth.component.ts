import { Component, OnInit } from '@angular/core';
import { AuthService } from '../services/auth.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.css']
})
export class AuthComponent implements OnInit {

  firstname;
  familyname;
  username;
  password;
  invalidLogin = false;
  showRegisterBtn = false;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private authService: AuthService) { }

  ngOnInit() {
  }

  register() {
    const outputField = document.getElementById('api-output');
    this.authService.register({firstName: this.firstname, familyName: this.familyname, username: this.username, password: this.password})
      .subscribe(response => {
        if (response) {
          console.log(response);
          outputField.innerHTML = JSON.stringify(response);
          this.router.navigate(['ninja']);
        }
      }, (err) => {
        console.log('registeration failed');
        outputField.innerHTML = JSON.stringify(err.json());
        console.log(err);
      });
  }
  login() {
    // console.log(this.username + ' ' + this.password);

    this.authService.login({username: this.username, password: this.password})
      .subscribe(result => {
      if (result) {
        const returnUrl = this.route.snapshot.queryParamMap.get('returnUrl');
        this.router.navigate([returnUrl || 'ninja']);
      } else {
        console.log('invalid login');
        this.invalidLogin = true;
      }
    }, (error) => {
        this.invalidLogin = true;
    });
  }

  getFreeInfo() {
    this.router.navigate(['free']);
  }

  toggleLoginBtn() {
    this.showRegisterBtn = this.showRegisterBtn ? false : true;
  }
}
