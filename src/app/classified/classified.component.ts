import { Component, OnInit } from '@angular/core';
import { ResourcesService } from '../services/resources.service';
import { AuthService } from '../services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-classified',
  templateUrl: './classified.component.html',
  styleUrls: ['./classified.component.css']
})
export class ClassifiedComponent implements OnInit {

  classifiedInfo = '';
  constructor(
    private resService: ResourcesService,
    private authService: AuthService,
    private router: Router) { }

  ngOnInit() {
  }

  getNinjaReport() {
    console.log('ninja report requested');
    const outputField = document.getElementById('api-output');
    this.resService.getClassifiedInfo()
    .subscribe(res => {
      outputField.innerHTML = JSON.stringify(res);

      this.classifiedInfo = res.text();
    },
    err => {
      outputField.innerHTML = JSON.stringify(err);
    });
  }

  logout() {
    this.authService.logout();
    this.router.navigate(['auth']);
  }
}
