import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'ninja-app';
  fullUrl = 'http://localhost/UserAccountDemo/information/free';

  hostChanged(inputField: HTMLInputElement) {
    const inputValue = inputField.value;
    this.fullUrl = inputValue + '/information/free';
  }

  goToURL() {
    const href = this.fullUrl;
    Object.assign(document.createElement('a'), {
      target: '_blank',
      href,
    }).click();
  }
}
