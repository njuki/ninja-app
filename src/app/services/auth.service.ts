import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { map, catchError } from 'rxjs/operators';
import { Observable, of } from 'rxjs';


@Injectable()
export class AuthService {
  // private url = 'http://localhost/UserAccountDemo/user/';

  constructor(private http: Http) { }
  register(credentials: {}) {
    console.log('registering...');
    console.log(this.getUrl());

    return this.http
      .post(this.getUrl() + 'register', credentials)
      .pipe(
        map(response => {
          if (response.text() !== null) {}
          localStorage.setItem('token', response.text());
          return response;
        })
      );
  }

  login(credentials: {}) {
    console.log('logging in user...');

    return this.http
      .post(this.getUrl() + 'login', credentials)
      .pipe(
        map(response => {
        if (response) {
          localStorage.setItem('token', response.text());
          return true;
        }
        return false;
      }));
  }

  getUrl() {
    const hostEl = document.getElementById('host') as HTMLInputElement;
    return hostEl.value + '/user/';
  }

  logout() {
    localStorage.removeItem('token');
  }

  isLoggedIn() {
    const token = localStorage.getItem('token');
    if (token) {
      return true;
    }
    return false;
  }
}
