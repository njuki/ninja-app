import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import { HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable()
export class ResourcesService {
  // private url = 'http://localhost/UserAccountDemo/information/';

  constructor(private http: Http) {}
  getGeneralInfo() {
    return this.http.get(this.getUrl() + 'free');
    // .pipe(map(response => response.json()));
  }

  // doesn't work for some reason
  // todo: figure out why the token isn't getting sent to api
  getClassifiedInfo() {
    const headers = new Headers({
      'Content-Type': 'application/json'
    });
    const authHeader = 'Bearer ' + localStorage.getItem('token');
    headers.append('Authorization', authHeader);
    console.log(headers);
    return this.http.get(this.getUrl() + 'protected', {headers});
  }

  getUrl() {
    const hostEl = document.getElementById('host') as HTMLInputElement;
    return hostEl.value + '/information/';
  }
}
