import { Component, OnInit } from '@angular/core';
import { ResourcesService } from '../services/resources.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-general',
  templateUrl: './general.component.html',
  styleUrls: ['./general.component.css']
})
export class GeneralComponent implements OnInit {
  generalInfo;
  constructor(private resService: ResourcesService, private router: Router) {}

  ngOnInit() {
    this.resService.getGeneralInfo()
    .subscribe(res => {
      const outputField = document.getElementById('api-output');
      outputField.innerHTML = JSON.stringify(res);

      this.generalInfo = res.text();
    });
  }

  goToAuth() {
    this.router.navigate(['auth']);
  }
}
