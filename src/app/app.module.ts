import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule, Http } from '@angular/http';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AuthComponent } from './auth/auth.component';
import { GeneralComponent } from './general/general.component';
import { ClassifiedComponent } from './classified/classified.component';
import { AuthService } from './services/auth.service';
import { AuthGuard } from './services/auth-guard.service';
import { ResourcesService } from './services/resources.service';

@NgModule({
  declarations: [
    AppComponent,
    AuthComponent,
    GeneralComponent,
    ClassifiedComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpModule,
    FormsModule
  ],
  providers: [
    AuthService,
    AuthGuard,
    ResourcesService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
