import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthComponent } from './auth/auth.component';
import { GeneralComponent } from './general/general.component';
import { ClassifiedComponent } from './classified/classified.component';
import { AuthGuard } from './services/auth-guard.service';


const routes: Routes = [
  { path: '', redirectTo: 'free', pathMatch: 'full' },
  { path: 'auth', component: AuthComponent },
  { path: 'free', component: GeneralComponent },
  { path: 'ninja', component: ClassifiedComponent, canActivate: [AuthGuard] }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
